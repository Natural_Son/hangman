﻿using System;
using System.Collections.Generic;
using static System.Random;
using System.Text;

namespace HangmanGame
{
    internal class NewBaseType
    {
        private static void printHangman(int wrong)
        {
            if(wrong == 0)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine("    |");
                Console.WriteLine("    |");
                Console.WriteLine("    |");
                 Console.WriteLine("   ===");
            }
           else if(wrong == 1)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine("o    |");
                Console.WriteLine("     |");
                Console.WriteLine("     |");
                Console.WriteLine("     ===");
            }
            else if(wrong == 2)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine("o    |");
                Console.WriteLine("|    |");
                Console.WriteLine("     |");
                Console.WriteLine("    ===");
            }
            else if(wrong == 3)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine(" o   |");
                Console.WriteLine("/|   |");
                Console.WriteLine("     |");
                Console.WriteLine("    ===");
            }
            else if(wrong == 4)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine(" o   |");
                Console.WriteLine("/|\\  |");
                Console.WriteLine("     |");
                Console.WriteLine("    ===");
            }
            else if(wrong == 5)
            {
                Console.WriteLine("\n+---+");
                Console.WriteLine(" o   |");
                Console.WriteLine("/|\\  |");
                Console.WriteLine("/    |");
                Console.WriteLine("    ===");
            }
             else if(wrong == 6)
            {     
                Console.WriteLine("\n+---+");
                Console.WriteLine(" o   |");
                Console.WriteLine("/|\\   |");
                Console.WriteLine("/ \\   |");
                Console.WriteLine("     ===");
            }
        }
  
        private static int printWord(List<char>guessedLetters, String Randomword)
        {
            int counter = 0;
            int rightletters = 0;
            Console.Write("\r\n");
            foreach (char c in Randomword)
            {
                if (guessedLetters.Contains(c))
                {
                    Console.Write(c + " ");
                    rightletters++;
               }
               else
               {
                    Console.Write(" ");
               }
               counter++;
            }
            return rightletters;
        }

        private static void printLines(string Randomword)
        {
            Console.Write("\r");
            foreach (Char c in Randomword)
            {
                Console.OutputEncoding = System.Text.Encoding.Unicode;
                Console.Write("\u0305 ");
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Welcome to hangman :)");
            Console.WriteLine("--------------------------------");

            Random random = new Random();
           List<string> wordDictionary =new List<string> { "Jesus","Thrill","Saturday","Music","Mentally","Weed","Network","life"};
            int index = random.Next(wordDictionary.Count);
            String Randomword = wordDictionary[index];

            foreach (char x in Randomword)
            {
                Console.Write(" # ");
            }

            int LengthOfWordToGuess = Randomword.Length;
            int AmountOfTimesWrong = 0;
            List<char> CurrentLettersGuessed = new List<char>();
            int CurrentLettersRight = 0;

            while(AmountOfTimesWrong != 6 && CurrentLettersRight != LengthOfWordToGuess)
            {
                Console.Write("\nletter guessed so far; ");
                foreach(char letter in CurrentLettersGuessed)
                {
                    Console.Write(letter + " ");
                } 
                // promt user for input
                Console.Write("\nGuess a Letter: ");
                char letterGuessed = Console.ReadLine()[0];

                if (!CurrentLettersGuessed.Contains(letterGuessed))
                {
                    Console.Write("\nLetter you guessed is not correct try again! ");
                }
                    // Check if letter has already beeen guessed
                    if (CurrentLettersGuessed.Contains(letterGuessed))
               {
                 Console.Write("\r\nYou have already guessed this letter." + letterGuessed); 
                printHangman(AmountOfTimesWrong);
                CurrentLettersRight = printWord(CurrentLettersGuessed, Randomword);
                printLines(Randomword);
            }
            else
            {
                // Check if letter is in word
                bool right = false;
                for(int i = 0; i < Randomword.Length; i++) {if (letterGuessed.ToString().ToLower() == Randomword[i].ToString().ToLower()) { right = true; } }

                if (right)
                {
                    printHangman(AmountOfTimesWrong);
                    CurrentLettersGuessed.Add(letterGuessed);
                    CurrentLettersRight = printWord(CurrentLettersGuessed, Randomword);
                    Console.Write("\r\n");
                    printLines(Randomword);
                }
                else
                {
                    AmountOfTimesWrong++;
                    CurrentLettersGuessed.Add(letterGuessed);
                    printHangman(AmountOfTimesWrong);
                    CurrentLettersRight = printWord(CurrentLettersGuessed, Randomword);
                    Console.Write("\r\n");
                    printLines(Randomword);
                }
            


            }
        }
        Console.WriteLine("\r\nGGame is over! Thank you for playing :)");
    }
  }    
}